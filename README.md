# initialuserservice
The purpose of this project is to be used as an initial starter image project for "SmartCLIDE CI/CD"(or others) and does not include any CI/CD or any other container support/configuration. We can imagine it as a new project that have not been included to any git repo yet. 

Basically it will simulate the flow process of creating a user generated service/project/code from SmartCLIDE-jbpm and SmartCLIDE service creation 
and the step of adding CI/CD and container support.
